#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 06:57:55 2019

@author: kendalljohnson
"""

# Homework 8
print("Homework 8 Question 2 part c")

# Imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# ODE Function 
def f(t,y):
    y_prime=-y+t*y**(1/2)
    return y_prime




# IC Varibles 
t0=2
yi=2
t= np.linspace(0,1,100)
ti = 2
tf = 3
delta_t=np.array([.25,.1,.05,.01])
n=len(delta_t)

# Main Body
lab=np.array(["dt=.25","dt=.1","dt=.05","dt=.01"])
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)
    y  = np.zeros(nt)
    y[0] = yi
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * f(t[k],y[k])
    plt.plot(t,y,label=lab[i])
    
# Scipy ODEint
dydt=lambda y,t:-y+t*y**(1/2)
sol = odeint(dydt,yi,t)
  
# Plotting 
plt.plot(t,sol,label="ODEint")
plt.legend()
plt.xlabel("t")
plt.ylabel("y")
plt.grid()
plt.title("Solutions to y_prime=-y+ty^(1/2)")