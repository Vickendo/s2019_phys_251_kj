#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 11:18:00 2019

@author: kendalljohnson
"""

# Homework 8
print("Homework 8 Question 1 part a")

# Imports 
import numpy as np
import matplotlib.pyplot as plt

# ODE Function 
def f(t,y):
    y_prime=t*np.exp(3*t)-2*y
    return y_prime

# IC Varibles 
t0=0
yi=0
t= np.linspace(0,1,100)
ti = 0
tf=1
delta_t=np.array([.5,.1,.05,.01])
n=len(delta_t)

# Main Body
lab=np.array(["dt=.5","dt=.1","dt=.05","dt=.01"])
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)
    y  = np.zeros(nt)
    y[0] = yi
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * f(t[k],y[k])
    plt.plot(t,y,label=lab[i])
    
# Plotting
plt.legend()
plt.xlabel("t")
plt.ylabel("y")
plt.grid()
plt.title("Solutions to y_prime=te^(3t)-2y")