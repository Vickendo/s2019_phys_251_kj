#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 23:17:56 2019

@author: kendalljohnson
"""

# Homework 8
print("Homework 8 Question 1 part c")
# Imports 
import numpy as np
import matplotlib.pyplot as plt

# ODE Function 
def f(t,y):
    y_prime=1+y/t
    return y_prime

# IC Varibles 
ti=2
yi=0
tf=3
t= np.linspace(ti,tf,100)
delta_t=np.array([.25,.1,.05,.01])
n=len(delta_t)
# Main Body
lab=np.array(["dt=.25","dt=.1","dt=.05","dt=.01"])
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)
    y  = np.zeros(nt)
    y[0] = yi
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * f(t[k],y[k])
    plt.plot(t,y,label=lab[i])

# Plotting  
plt.legend()
plt.xlabel("t")
plt.ylabel("y")
plt.grid()
plt.title("Solutions to y_prime=1+y/t")