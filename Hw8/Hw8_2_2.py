
# Homework 8
print("Homework 8 Question 2 part b")

# Imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# ODE Function 
def f(t,y):
    y_prime=(t**(2))*(np.sin(2*t)-2*t*y)
    return y_prime

# IC Varibles 
t0=1
yi=2
t= np.linspace(0,1,100)
ti = 1
tf = 2
delta_t=np.array([.5,.1,.05,.01])
n=len(delta_t)

# Main Body
lab=np.array(["dt=.5","dt=.1","dt=.05","dt=.01"])

# Scipy ODEint
dydt=lambda y,t:(t**(2))*(np.sin(2*t)-2*t*y)
sol = odeint(dydt,yi,t)

for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)
    y  = np.zeros(nt)
    y[0] = yi
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * f(t[k],y[k])
    plt.plot(t,y,label=lab[i])
  
# Plotting   
plt.plot(t,sol,label="ODEint")
plt.legend()
plt.xlabel("t")
plt.ylabel("y")
plt.grid()
plt.title("Solutions to y_prime=t^2*sin(2t)-2ty")