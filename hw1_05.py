#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:53:36 2019

@author: kendalljohnson
"""
# Question 5 of Homework 1
import math

def Sat_height(t):  
    pi=math.pi
    g=6.67*(10**-14)           # Gravitational Constant km**3/kg*(s**2) // And dived by an extra 1000
    m=5.97*(10**24)            # Mass of Earth in kilograms
    R=6371                     # Radius of Earth in kilometers
    G =g*m*(t**2)              # Top part of the equation 
    U= 4*(pi**2)               # Bottem part of the equation 
    H=G/U                      # Act of division
    D= (H**(1/3))              # Exponant of 1/3
    h= (D-R)/100               # Subtracted Radius R from the current equation and Divided by 100 for some reason

    #h=(((g*m*(t**2))/(4*(pi**2)))**(1/3))- R
    #orginal equation but got wrong answer before creating tons of varibles for indivual eqautions, but the result seemed to have wrong in the answer
    return h
# T is in seconds
print("If the object takes {0:} hours to circle the Earth it is {1:10f} kilometers above Earth's Surface".format(24,Sat_height(86400)))
print("If the object takes {0:} minutes to circle the Earth it is {1:10f} kilometers above Earth's Surface".format(90,Sat_height(5400)))
print("If the object takes {0:} minutes to circle the Earth it is {1:10f} kilometers above Earth's Surface".format(45,Sat_height(2700)))
#I simply converted the given units of time it to seconds for the print statments
print("The Moon takes {0:} seconds to circle the Earth it is {1:10f} kilometers above Earth's Surface".format(2332800,Sat_height(2332800)))
# I added the moon for acceraciey, NASA says Lunar dis is 384402 km and python predicted 379978 km
S= Sat_height(86400)- Sat_height(86160)
print('The difference in height between the geosynchronous and sidereal satellite orbits is {:10f} kilometers'.format(S))