#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 10:58:09 2019

@author: kendalljohnson
"""

# Question 1 from homework 5
print()
print('Question 3 from homework 5')

# Global Varibles
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm 
from mpl_toolkits.mplot3d import Axes3D

# Importing data
dname='data_phys251'
fname='data_2D_grid_T.txt'                   
dfname ='{0:s}/{1:s}'.format(dname,fname)    
data01=np.genfromtxt(dfname,delimiter='',skip_header=13)   
x = data01[:,0]
y= data01[:,1]
z = data01[:,2]
T = data01[:,3]

# Manipulations
y=10.5+y
x=10.5+x
# Part 1 : Graphing

# Varibles
num=41
X=np.reshape(x,[num,num])
Y=np.reshape(y,[num,num])
Z=np.reshape(z,[num,num])

# Plotting

#fig = plt.figure()
#ax = fig.gca(projection='3d')
#surf = ax.plot_surface(X, Y, Z, cmap=cm.terrain,linewidth=0, antialiased=False,label=' 3D surface part 1')
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')
#ax.set_title('Graph 1')
#plt.show()
###############################################################################

# Part 2

# Varibles and Arrays
siz=x.size
xmax=10
xmin=-10
ymax=10
ymin=-10
dx=.5
dy=.5
nx=((xmax-xmin)/dx)+1
ny=((ymax-ymin)/dy)+1
# Empty Arrays
a=np.empty([siz,siz],dtype='float')
b=np.empty([siz,siz],dtype='float')
c=np.empty([siz,siz],dtype='float')
d=np.empty([siz,siz],dtype='float')
z1=np.empty([siz,siz],dtype='float')
zzz=np.empty([siz,siz],dtype='float')
# Use of Meshgrid
xx,yy=np.meshgrid(x,y)
# For loops
for i in range(siz-1):
    for j in range(siz-1):    
        a=z1[i,j]*(x[i+1]-nx)*(y[j+1]-ny)
        b=z1[i,j+1]*(x[i+1]-nx)*(ny-y[j])
        c=z1[i+1,j]*(nx-x[i])*(y[j+1]-ny)
        d=z1[i+1,j+1]*(nx-x[i])*(ny-y[j])
       
# function for Equation 2 defined in HW 5
def f(x,y):
    D=a+b+c+d
    zz=D/(xx*yy)
    return zz
    
zzz=np.reshape(zz,[siz,1])

for i in range(siz):
   for j in range(siz):
        zzz[i]=zz[j] ##--fix right the first time
        zzz[i,j]=zz[j]   
    

# Plot the surface
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(xx, yy, zzz, cmap=cm.terrain,linewidth=0, antialiased=False)
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
plt.show()

# Plot features
#ax.plot(x, y, z, label='parametric curve')
#plt.legend()