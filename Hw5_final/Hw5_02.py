#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 20:00:49 2019

@author: kendalljohnson
"""
# Question 2 from homework 5

print()
print('Question 2 from homework 5')

# Global Varibles

import numpy as np
import matplotlib.pyplot as plt

# Importing data

dname='data_phys251'
fname='fc_thist_00520'                   
dfname ='{0:s}/{1:s}'.format(dname,fname)    
data01=np.genfromtxt(dfname,skip_header=2)   
time = data01[:,0]
pr = data01[:,1]
u =data01[:,2]
v =data01[:,3]
w =data01[:,4]
V =data01[:,5]
t =data01[:,6] #is all zero
s1 =data01[:,7]
s2 =data01[:,8]
s3 =data01[:,9]
s4 =data01[:,10] #is all zero
s5 =data01[:,11] #is all zero

# Array sizes
ttt=time.size
ss1=s1.size

# Part 1: Slicing

T=[]
S=[]

for i in range(0,ttt,400):
    T.append(time[i])

for i in range(0,ss1,400):
    S.append(s1[i])


# Linear Interpolation 1 (LI1)
    
Ta=len(T)
T_int1=np.empty([Ta],dtype='float')
S_int1=[]
for i in range(Ta):   
    T_int1[-1]=0 
    T_int1[i]=(T[i]+T[i-1])/2

for i in range(Ta):
    S_int= S[i]+(T_int1[i]-T[i-1])*(S[i]-S[i-1])/(T[i]-T[i-1])
    S_int1.append(S_int)
    
# More Slicing / first value of both is an outlier due to form of linear equation
TI1=T_int1[1:]

SI1=S_int1[1:]


# Part 2 : 1 sec  slicing is 240,000/400=600 incraments in one unit
T1=[]
S1=[]

for i in range(0,ttt,600):
    T1.append(time[i])

for i in range(0,ss1,600):
    S1.append(s1[i])

# Linear Interpolation 2 (LI2)
    
Ta=len(T1)
T_int2=np.empty([Ta],dtype='float')
S_int2=[]
for i in range(Ta):   
    T_int2[-1]=0 
    T_int2[i]=(T1[i]+T1[i-1])/2

for i in range(Ta):
    S_int_2= S1[i]+(T_int2[i]-T1[i-1])*(S1[i]-S1[i-1])/(T1[i]-T1[i-1])
    S_int2.append(S_int_2)
    
# More Slicing / first value of both is an outlier due to form of linear equation
TI2=T_int2[1:]

SI2=S_int2[1:]



# Part 3 : 10 sec slicing is 240,000/40=6000 incraments in one unit
T2=[]
S2=[]

for i in range(0,ttt,6000):
    T2.append(time[i])

for i in range(0,ss1,6000):
    S2.append(s1[i])

# Linear Interpolation 3 (LI3)
    
Ta=len(T2)
T_int3=np.empty([Ta],dtype='float')
S_int3=[]
for i in range(Ta):   
    T_int3[-1]=0 
    T_int3[i]=(T2[i]+T2[i-1])/2

for i in range(Ta):
    S_int_3= S2[i]+(T_int3[i]-T2[i-1])*(S2[i]-S2[i-1])/(T2[i]-T2[i-1])
    S_int3.append(S_int_3)
    
# More Slicing / first value of both is an outlier due to form of linear equation
TI3=T_int3[1:]

SI3=S_int3[1:]
print(T2)
# Plotting
    
# Plot 1

plt.plot(T,S,'b',label='Raw Data')
#plt.plot(TI1,SI1,'r',label='Lin Int 1')

# Plot 2

#plt.plot(T1,S1,'b',label='Raw Data (1 sec)')
#plt.plot(TI2,SI2,'r',label='Lin Int 2')

# Plot 3

#plt.plot(T2,S2,'b',label='Raw Data (10 sec)')
plt.plot(TI3,SI3,'r',label='Lin Int 3')

plt.xlabel('Time in Seconds(s)')
plt.ylabel('The S1 of H2S concentrate ppm')
plt.grid()
plt.title("H2S concentrate ppm")
plt.legend()