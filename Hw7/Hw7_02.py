#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 16:32:10 2019

@author: kendalljohnson
"""

# Homework 7 question 2
print("Homework 7 Question 2")

# Imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import derivative as d

# Fuction Defining of the equation f(x)= sin(x)/x
def f(x):
    f=np.sin(x)/x
    return f

# Varibles and For loop
    
x=np.zeros(200)             # Blank array of 200
for i in range(200):        # fill x array will correct x values
    x[i]=5+ 25*(i/200)
     
tx=x[1]-x[0]                # change in x  tx= .125
y=f(x)                      # function array



# Analytical Derivative using scipy
Af=d(f,x,tx)

# Foward Derivative
Ff=(f(x+tx)-f(x))/tx

# Backward Derivative
Bf=(f(x)-f(x-tx))/tx

# Center Derivative
F_prime = (f(x+tx)-f(x-tx))/(2*tx)

# Difference in derivative: simply to check accuracy 

# Foward
O1 = (Ff[10]-Af[10])        # i = 10 is abitary 
# Backward
O2 = (Bf[10]-Af[10])
# Central
O3 = (F_prime[10]-Af[10])

# Plotting
plt.plot(x,y,'.',label="Function")     # The plot of the function of x
plt.plot(x,Af,'.',label="Analytical")  # The plot of the derivative of the function of x using scipyx
plt.plot(x,Ff,'.',label="Foward"      )# The plot of the approximatited derivative of the function of x using Forward Method
plt.plot(x,Bf,'.',label="Backward")    # The plot of the approximatited derivative of the function of x using Backward Method
plt.plot(x,F_prime,'.',label="Center") # The plot of the approximatited derivative of the function of x using Center Method
plt.legend()
plt.xlabel("X-value (5 to 30)")
plt.ylabel("F(x) and F'(x)")
plt.grid()
plt.title("Approx Differential of F(x)=sin(x)/x")