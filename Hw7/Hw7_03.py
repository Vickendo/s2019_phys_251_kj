#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 10:01:09 2019

@author: kendalljohnson
"""

# Homework 7 question 3
print("Homework 7 Question 3")

# Imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import derivative as d

# Fuction Defining 
def f(x):
    A=np.exp(-x**2)
    return A

# Varibles 
x=np.linspace(-5,5,200)  # used linspace instead of for - loop 
tx=x[1]-x[0]  # change in x  tx= .05
y=f(x)        # function array

# Analytical Derivative
Af=d(f,x,tx)      # 1st Derivative
A2f=d(f,x,tx,n=2) # 2nd Derivative

# Foward Derivative
Ff=(f(x+tx)-f(x))/tx

# Backward Derivative
Bf=(f(x)-f(x-tx))/tx

# Center Derivative
F_prime = (f(x+tx)-f(x-tx))/(2*tx)

# Second Derivative
F_2x_prime = (f(x+tx)-2*f(x)+f(x-tx))/(tx**2)

# Difference in derivative: to check accuracy 

# Foward
O1 = (Ff[10]-Af[10])    # i = 10 is abitary in this case
# Backward
O2 = (Bf[10]-Af[10])
# Central
O3 = (F_prime[10]-Af[10])
# Second Derivative
O4 = (F_2x_prime[10]-A2f[10])

# Plotting
plt.plot(x,y,'.',label="Function")          # The plot of the function of x
plt.plot(x,Af,'x',label="Analytical 1st" )  # The plot of the derivative of the function of x using scipy
plt.plot(x,A2f,'x',label="Analytical 2nd")  # The plot of the 2nd derivative of the function of x using scipy
plt.plot(x,Ff,'.',label="Foward")           # The plot of the approximatited derivative of the function of x using Forward Method
plt.plot(x,Bf,'.',label="Backward")         # The plot of the approximatited derivative of the function of x using Backward Metho
plt.plot(x,F_prime,'.',label="Center")      # The plot of the approximatited derivative of the function of x using Center Method
plt.plot(x,F_2x_prime,'.',label="2nd Der Center")# The plot of the approximatited 2nd derivative of the function of x using Center Method
plt.legend()
plt.xlabel("X-value (-5 to 5)")
plt.ylabel("F(x),F'(x),and F''(x)")
plt.grid()
plt.title("Approx Differential of F(x)= e^(-x^2)")