#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 16:32:10 2019

@author: kendalljohnson
"""

# Homework 7 question 1
print("Homework 7 Question 1")

# Imports 
import numpy as np
from math import pi
import matplotlib.pyplot as plt
from scipy.misc import derivative as d

# Fuction Defining of the equation f(x)=sin(x)+cos(x)
def f(x):
    f=np.cos(x)+np.sin(x)
    return f

# Varibles / For loop
x=np.zeros(100)
for i in range(100):
    x[i]=(i/100)*2*pi
     
tx=x[1]-x[0]  # change in x  tx = .0626
y=f(x)        # function array

# Analytical Derivative using scipy
Af=d(f,x,tx)

# Approx Foward Derivative
Ff=(f(x+tx)-f(x))/tx

# Approx Backward Derivative
Bf=(f(x)-f(x-tx))/tx

# Approx Center Derivative
F_prime = (f(x+tx)-f(x-tx))/(2*tx)

# Difference in derivatives: simply to check accuracy 

O1 = (Ff[10]-Af[10])
O2 = (Bf[10]-Af[10])
O3 = (F_prime[10]-Af[10])


# Plotting

plt.plot(x,y,'.',label="Function")     # The plot of the function of x
plt.plot(x,Af,'*',label="Analytical"  )# The plot of the derivative of the function of x using scipy
plt.plot(x,Ff,'.',label="Foward")      # The plot of the approximatited derivative of the function of x using Forward Method
plt.plot(x,Bf,'.',label="Backward")    # The plot of the approximatited derivative of the function of x using Backward Method
plt.plot(x,F_prime,'x',label="Center") # The plot of the approximatited derivative of the function of x using Center Method
plt.legend()
plt.xlabel("X-value (0 to 2 pi)")
plt.ylabel("F(x) and F'(x)")
plt.grid()
plt.title("Approx Differential of F(x)=sin(x)+cos(x)")