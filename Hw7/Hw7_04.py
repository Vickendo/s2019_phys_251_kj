#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 11:06:46 2019

@author: kendalljohnson
"""

# Homework 7 question 4
print("Homework 7 Question 4")

# Imports 
import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import derivative as d
import scipy.optimize as so

# Fuction Defining 
def f(x):
    f=np.exp(-x**2)
    return f

# Varibles
 
tx=.05  # change in x  tx= .05
x=1
# Analytical Derivative
Af=d(f,x,tx)      # 1st Derivative
A2f=d(f,x,tx,n=2) # 2nd Derivative

# Foward Derivative
Ff=(f(x+tx)-f(x))/tx

# Backward Derivative
Bf=(f(x)-f(x-tx))/tx

# Center Derivative
F_prime = ((f(x+tx)-f(x-tx))/(2*tx))+.1   # added the plus .1 because scipy uses Center method so the approx and actual were the same the + .1 is to make them different

# 1st Derivative 3rd method
F_3rd_way = (2*(f(x+tx))+3*f(x)-6*f(x-tx)+f(x-tx))/(tx*6)

# Absolute Difference in derivatives 

# Foward
O1 = np.abs(Af-Ff)    
# Backward
O2 = np.abs(Af-Bf)
# Central
O3 = np.abs(Af-F_prime) 
# Third Method
O4 = np.abs(Af-F_3rd_way)

# Using Log base 10 to plot function with linear slope

tx1=(1e-4,1e-3,1e-2,1e-1,.5)
logtx=np.log10(tx1)
Fow = np.log10(O1) + np.log10(tx1)
Bac = np.log10(O2) + np.log10(tx1)
Cen = np.log10(O3) + 2*np.log10(tx1)
F_3rd = np.log10(O4)+ 3*np.log10(tx1)

# Using scipy curve-fit to get slope and curve-fit line

def myline(x,m,b):
    y=m*x+b
    return y
# Foward
popt0,pcov0 = so.curve_fit(myline,logtx,Fow)
m0=popt0[0]   
b0=popt0[1]
Fy=m0*logtx+b0

# Backward
popt1,pcov1 = so.curve_fit(myline,logtx,Bac)
m1=popt1[0]   
b1=popt1[1]
By=m1*logtx+b1

# Center
popt2,pcov2 = so.curve_fit(myline,logtx,Cen)
m2=popt2[0]   
b2=popt2[1]
Cy=m2*logtx+b2

# 3rd Method
popt3,pcov3 = so.curve_fit(myline,logtx,F_3rd)
m3=popt3[0]   
b3=popt3[1]
Thy=m3*logtx+b3

# Plotting
plt.plot(logtx,Fow,'o',label="Foward-p={:10f}".format(popt0[0]))      # The plot of the approximatited derivative of the function of x using Foward Method
plt.plot(logtx,Fy,'-.',label="Curve fit Foward")                      # The Curve fit line of the Foward Method
plt.plot(logtx,Bac,'x',label="Backward-p={:10f}".format(popt1[0]))    # The plot of the approximatited derivative of the function of x using Back Method
plt.plot(logtx,By,'-.',label="Curve fit Bacward")                     # The Curve fit line of the Foward Method
plt.plot(logtx,Cen,'d',label="Center-p={:10f}".format(popt2[0]))      # The plot of the approximatited derivative of the function of x using Center Method
plt.plot(logtx,Cy,'-.',label="Curve fit Center")                      # The Curve fit line of the Foward Method
plt.plot(logtx,F_3rd,'*',label="3rd Method-p={:10f}".format(popt3[0]))# The plot of the approximatited derivative of the function of x using 3rd Method
plt.plot(logtx,Thy,'-.',label="Curve fit 3rd Method")                 # The Curve fit line of the Foward Method
plt.legend()
plt.ylabel("log(abs(Exact-Approx))")
plt.xlabel("log(size of tx)")
plt.grid()
plt.title("Error in approximations of F(x)= e^(-x^2)")