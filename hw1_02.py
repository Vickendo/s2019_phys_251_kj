#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:13:01 2019

@author: kendalljohnson


"""
# Question 2 of homework 1

import math       # import the math function

type(1)           # data type is a int

type(1.)          # data type is a float

type(1.0)         # data type is a float

p = math.pi
type(p)           # data type is a float

e = math.e
type(e)           # data type is a float

type(1 + p)       # data type is a float

type(2)           # data type is a int

type(2/3)         # data type is a float

j=1.2
type(4-j)         ## data type is a float