#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 22:00:15 2019

@author: kendalljohnson
"""
# Sum of Even Fibonacci numbers
Fib=[0,1]
for i in range(100000):
    num=Fib[i]+Fib[i+1]
    if num > 4000000:
        break
    Fib.append(num)
    
print(Fib)
print()
new_Fib=[]
for i in Fib:
    if i%2==0:
        new_Fib.append(i)
print(new_Fib)
print("The Sum of The even Fib seq is {}".format(sum(new_Fib)))