#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 00:07:25 2019

@author: kendalljohnson
"""
# Square of sum - sum of squares
sum_sq=0
squ_sum= 0
for i in range(101):
    sum_sq += i**2
    squ_sum+=i
       
sumsum=(squ_sum**2)-sum_sq
print("the answer is {}".format(sumsum))