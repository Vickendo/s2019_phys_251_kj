#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 23:14:00 2019

@author: kendalljohnson
"""
# The largest palindromic 
big=0
for i in range(999,100,-1):
    for j in range(999,100,-1):
        num=i*j
        if(str(num)==str(num)[::-1]):
            if num > big:
                print(i,j,num)
                big=num
