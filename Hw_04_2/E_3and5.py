#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 11:09:20 2019

@author: kendalljohnson
"""
import numpy as np
import math
n=1000
b=np.empty([n],dtype='float')
c = []
for t in range(0,n):
    if t/3 == math.floor(t/3):
       c.append(t)
    elif t/5 == math.floor(t/5):
        c.append(t)
        
C=sum(c)
print(C)