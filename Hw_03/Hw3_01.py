#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 11:26:51 2019

@author: kendalljohnson
"""
#import mods
from math import pi
import numpy as np
import matplotlib.pyplot as plt

#Question 1 part 1
print('Question 1 part 1')
print() #extra space for clarity 

#making dependent varible a list
t=np.linspace(-pi,pi,50)

#Equation set up for graph of function 1
f=np.cos(2.0*t)

#Ploting steps
plt.plot(t,f,label='f(x)=cos(2t)',color='r')
plt.grid()
plt.title('F(t) = cos ( 2t )')
plt.xlabel('Angle(theta)')
plt.ylabel('Length')
plt.legend()
plt.show()
plt.savefig("f(x)=cos(2t).png")

############################################

#Question 1 part 2
print() #extra space for clarity 
print('Question 1 part 2')
print() #extra space for clarity 

#Equation set up for graph of function 2
v=(2*((np.cos(t))**2))-1

#Ploting steps
plt.plot(t,v,label='f(x)=cos(t^2)-1',color='g')
plt.title('F(t) = 2cos(t^2)-1')
plt.xlabel('Angle(theta)')
plt.ylabel('Length')
plt.grid()
plt.legend()
plt.show()
plt.savefig("f(x)=cos(t^2)-1.png")

############################################

#Question 1 part 3
print() #extra space for clarity 
print('Question 1 part 3')
print() #extra space for clarity 

#Equation set up for graph of function 3 
w= (1/(np.cos(t))) + np.tan(t)

#Ploting steps
plt.plot(t,w,label='f(x)=sec(t) - tan(t)',color='b')
plt.legend()
plt.title('F(t) = sec (t) - tan(t)')
plt.xlabel('Angle(theta)')
plt.ylabel('Length')
plt.grid()
plt.show()
plt.savefig("f(x)=sec(t) - tan(t).png")
