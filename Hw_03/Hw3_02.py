#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 22:06:08 2019

@author: kendalljohnson
"""
#import mods
import matplotlib.pyplot as plt
import numpy as np
from math import e

#Q 2.1 
print('Question 2 part 1')
print() #extra space for clarity 

#making dependent varible a list
x=np.linspace(-4,4,100)

#Functions
y1=np.sin(x)
y2=(x**2)

#Ploting steps
plt.plot(x,y1,'-',label='f(x)=sin(x)',color='k')
plt.plot(x,y2,'-',label='g(x)=x^2',color='b')
plt.title('f(x)=sin(x) and  g(x)=x^2')
plt.xlabel('Input')
plt.ylabel('Output')

#Used to put dot on intersection using difference in functions
idx = np.transpose(np.nonzero(np.diff(np.sign(y2 - y1)) != 0)).reshape(-1) + 0
plt.plot(x[idx], y1[idx], 'ro')
plt.grid()
plt.legend()
plt.show()
plt.savefig("f(x)=sin(x) and  g(x)=x^2.png")

#Intersections were found at (-0.1,0) and (0.8,0.3)

######################################

#Q 2.2
print() #extra space for clarity 
print('Question 2 part 2')
print() #extra space for clarity 
import matplotlib.pyplot as plt
import numpy as np

#Functions
ya=np.sin(2*x)
yb =((x**3)/10) + (x**2)/10

#Ploting steps
plt.plot(x,ya,'-',label='f(x)=sin(2x)',color='r',)
plt.plot(x,yb,'-',label='g(x)=(x^3)/10 + (x^2)/10',color='k')
plt.grid()

#Used to put dot on intersection using difference in functions
idt = np.argwhere(np.diff(np.sign(ya - yb)) != 0).reshape(-1) + 0
plt.plot(x[idt], ya[idt], 'ro')
plt.title('f(x)=sin(2x)  and  g(x)=(x^3)/10 + (x^2)/10')
plt.xlabel('Input')
plt.ylabel('Output')
plt.legend()
plt.show()
plt.savefig("Graph_2_Hw3_2.png")

#Intersections were found at (-1.7,0),(0,0) and (1.3,0.3)

#########################################
#Q 2.3
print() #extra space for clarity 
print('Question 2 part 3')
print() #extra space for clarity 
x1=np.linspace(-6,6,100)
x2=np.linspace(0.00001,6,100)   #made new varible for log to start from slightly bigger then zero

#Functions:: got complicated to deal with log because it was giving back an error for long runtime
#I was able to get rid of the run time issue with log like this (taking zero out of the dependent varable)
u1=0.001*x2
u2=x2**3
u3= np.log(u2)
u = e**u1+ u3
w = 0.1*x1**3 + 0.1*x1**2 -5

#Ploting steps
plt.plot(x2,u,'-',label='f(x)= e^(0.001x)+ ln(x^3)',color='c')
plt.plot(x1,w,'-',label='g(x) = 0.1x^3 + 0.1x^2 -5',color='k')
plt.grid()

#Used to put dot on intersection using difference in functions
idz = np.argwhere(np.diff(np.sign(u - w)) != 0).reshape(-1) + 0
plt.plot(x1[idz], w[idz], 'ro')
plt.title('f(x)=e^(0.001x)+ ln(x^3)  and  g(x)=0.1x^3 + 0.1x^2 -5')
plt.xlabel('Input')
plt.ylabel('Output')
plt.legend()
plt.show()
plt.savefig("f(x)=e^(0.001x)+ ln(x^3)  and  g(x)=0.1x^3 + 0.1x^2 -5.png")
#Intersections were found at (4.5,5) and (0.3,-5)