#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 12:30:51 2019

@author: kendalljohnson
"""
#importing mods
from math import pi
import numpy as np
import matplotlib.pyplot as plt

#Q 3.1
print('Question 3 part 1')
print() #extra space for clarity
#making usable fuctions

def x(t):
    a=(45*pi)/180
    x0=0
    v=40 #m/s
    X =x0+v*np.cos(a)*t
    return X

def y(t):
    a=(45*pi)/180
    g=9.8
    y0=0
    v=40 #m/s
    Y=y0+v*t*np.sin(a)-(1/2)*g*t**2
    return Y

#dependent var
t=np.linspace(0.0,7.0,100)

#Ploting steps
plt.plot(t,x(t),label='Horizontal',color='b')
plt.plot(t,y(t),label='Vertical',color='r')
plt.grid()
plt.title('Horizontal and vertical positions of thrown ball')
plt.xlabel('Time (seconds)')
plt.ylabel('Distance (meters)')
plt.legend()
plt.show()
plt.savefig("Horizontal and vertical positions of thrown ball.png")

#The ball hits the ground around 5.7 seconds
############################################

#Q 3.2
print() #extra space for clarity
print('Question 3 part 2')
print() #extra space for clarity
#making usable fuctions
def vx(t):
    t1 = np.ones(100) #because v(x) is constant I made an array of ones to plot it
    v=40*t1
    a=(45*pi)/180
    V=v*np.cos(a)
    return V

def vy(t):
    v=40
    a=(45*pi)/180
    g=9.8
    U= v*np.sin(a) -g*t
    return U

#Ploting steps
plt.plot(t,vx(t),label='Velocity of x-comp',color='b')
plt.plot(t,vy(t),label='Velocity of y-comp',color='r')
plt.title('Horizontal and vertical velocities of thrown ball')
plt.xlabel('Time (seconds)')
plt.ylabel('Velocity (meter per second)')
plt.legend()
plt.grid()
plt.show()
plt.savefig("Horizontal and vertical velocities of thrown ball.png")

#The velocity is 0 around 2.9 seconds

############################################

#Q 3.3
print() #extra space for clarity
print('Question 3 part 2')
print() #extra space for clarity
#Ploting steps
plt.plot(x(t),y(t),label='The Balls motion in space',color='k')
plt.xlabel('x-distance in meters')
plt.ylabel('y-distance in meters')
plt.grid()
plt.xlabel('Horzontal distance (meters)')
plt.ylabel('Vertical distance (meters)')
plt.title('Balls Path')
plt.legend()
plt.show()
plt.savefig("Balls Path.png")