#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:10:25 2019

@author: kendalljohnson
"""
#Question 1 of homework 1
import math
p = math.pi   # assigning pi using math function
e = math.e    # assigning e using math function
total = e + p # the sum of both varibles
print(total)  # solution answer is 5.859