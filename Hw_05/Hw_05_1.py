#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 11:33:02 2019

@author: kendalljohnson
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 10:19:41 2019

@author: kendalljohnson
"""
# Question 1 from homework 5

print()
print('Question 1 from homework 5')

# Global Varibles

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as so

# Importing data

dname='data_phys251'
fname='velocity_run018.txt'                   
dfname ='{0:s}/{1:s}'.format(dname,fname)    
data01=np.genfromtxt(dfname,delimiter=',',skip_header=1)   
t = data01[:,0]
v = data01[:,1]
dv =data01[:,2]

# Optimization curve fit line

def myline(t,m,b):
    v=m*t+b
    return v

# Scipy  Matrix
    
popt0,pcov0 = so.curve_fit(myline,t,v)
m0=popt0[0]   
b0=popt0[1]

# Scipy function

vhat=m0*t+b0

# Created variables and arrays

tt=t.size
t_half=np.empty([tt],dtype='float')
vt=[]

# Linear Interpolations

# For time values 
for i in range(0,tt):   
    t_half[0]=3.675 #Correction using first value of t data
    t_half[i]=(t[i-1]+t[i])/2
# For the velocity values
for j in range(0,tt):
    v_half = (v[j]+(t_half[j]-t[j-1])*(v[j]-v[j-1])/(t[j]-t[j-1]))-.1 # adjustment of minus .1
    vt.append(v_half)
    vt[0]=-1.7 #Correction using first value of v data

# Plotting
    
plt.plot(t,v,'.',label='Raw Data')
#plt.plot(t,vhat,'r',label='Curve fit line')
plt.plot(t_half,vt,'g',label='Linear Interpalation')
plt.xlabel("Time in Seconds(s)")
plt.ylabel("Velocity in meters per second (m/s)")
plt.grid()
plt.title("Velocity Run 18")
plt.errorbar(t,v,0,dv,'y')
plt.legend()

# Conclusion : linear interpolation fit the graph, but needed adjustment I needed to start arrays with the first number manually then a .1 translation to the right 