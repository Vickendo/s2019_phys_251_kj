#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:32:37 2019

@author: kendalljohnson
"""
#Question 4 of Homework 1

import math 

def sphereinfo(r):       # special function
    pi = math.pi         # varible pi
    v=pi*(4/3)*r**3      # Volume of Sphere Equation
    s=pi*4*r**2          # Surface area of Sphere Equation
    print("If the radius is {0:} meters the Volume of the sphere is {1:} meters cubed.".format(r,v))
    print("If the radius is {0:} meters the Surface Area of the sphere is {1:} meters squared.".format(r,s))

print(sphereinfo(10)) # I keep getting None at the end of the line and not sure why
print(sphereinfo(1))