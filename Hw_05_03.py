#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 10:58:09 2019

@author: kendalljohnson
"""

# Question 1 from homework 5
print()
print('Question 3 from homework 5')

# Global Varibles
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm 
from mpl_toolkits.mplot3d import Axes3D

# Importing data
dname='data_phys251'
fname='data_2D_grid_T.txt'                   
dfname ='{0:s}/{1:s}'.format(dname,fname)    
data01=np.genfromtxt(dfname,delimiter='',skip_header=13)   
x = data01[:,0]
y= data01[:,1]
z = data01[:,2]
T = data01[:,3]
y=10.5+y

# Part 1 

#zz=np.asarray([z,z],dtype='float')
#zz=(xx**2 + yy**2)
#zz=np.empty([siz,siz],dtype='float')
#for i in range(siz):
  # for j in range(siz):
        #zz[i]=z[j] ##--fix right the first tine
        #zz[i]=np.transpose(z[i])
        #zz[i,j]=z[j]
        #zz[i,j]=(np.cos(z[i]))**2 + (np.sin(z[j]))**2

#print(zz.shape)
#z_con=np.transpose(z)
#print(zz)
# Plot 3D surface
#zz = np.sin(xx**2 + yy**2)     
#ig = plt.figure()
#ax = fig.gca(projection='3d')
#surf = ax.plot_surface(xx, yy, zz, cmap=cm.terrain,linewidth=0, antialiased=False,label=' 3D surface part 1')
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')
#plt.show()

# Part 
xx,yy= np.meshgrid(x,y)
# Varibles 
siz=x.size
zz=np.empty([siz,siz],dtype='float')
xmax=10
xmin=-10
ymax=10
ymin=-10
dx=.5
dy=.5
nx=((xmax-xmin)/dx)+1
ny=((ymax-ymin)/dy)+1

a=np.empty([siz],dtype='float')
b=np.empty([siz],dtype='float')
c=np.empty([siz],dtype='float')
d=np.empty([siz],dtype='float')
z1=[]
for i in range(siz-1):
    for j in range(siz-1):    
        a=zz[i,j]*(x[i+1]-xx)*(y[j+1]-yy)
        b=zz[i,j+1]*(x[i+1]-xx)*(yy-y[j])
        c=zz[i+1,j]*(xx-x[i])*(y[j+1]-yy)
        d=zz[i+1,j+1]*(xx-x[i])*(yy-y[j])
        D=a+b+c+d
        if np.any(y[j] != 0 or x[i] != 0):
            z=(a+b+c+d)/(x[i+1]-x[i])*(y[j+1]-y[j])
            #z1.append(z)
#z1=np.isinf(z1)          
print(a)
print(b)
print(c)
print(d)
print(D)
#z2=[]
#for i in range(siz-1):
    #if z1[i] >1 or z1[i] <1:
       # z2.append(z1)
        
#print(z1)

    
# Plot the surface
#fig = plt.figure()
#ax = fig.gca(projection='3d')
#surf = ax.plot_surface(xx, yy, z1, cmap=cm.terrain,linewidth=0, antialiased=False)
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')
#ax.plot(x, y, z, label='Graph 2')
#plt.show()

# Plot features
#ax.plot(x, y, z, label='Graph 2')
#plt.legend()