#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:21:50 2019

@author: kendalljohnson
"""
# Question 3 of Homework 1

import math 


rad=10              # the radius of 10
p=math.pi           # the varible for pi
SA = 4 *p*(rad**2)  # Equation for the surface area of a sphere 
i2m=39.37           # Converstion varible from meter to inches
SAi= SA*i2m         # Converstion Eqaution from meter to inches
print("If the radius of a sphere is {0:} meters then the Surface area is {1:} meters squared".format(rad,SA))
print("If the radius of a sphere is {0:} meters then the Surface area is {1:} inches squared".format(rad,SAi))