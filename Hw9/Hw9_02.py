#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 20:30:31 2019

@author: kendalljohnson
"""

print("Homework 9 Question 2")

# Imports #####################################################################

import numpy as np
import matplotlib.pyplot as plt

# Defing the ODE Functions  

g= 9.8 # m/s^2 Earth's gravity constant
def F(t):  ###################### Eq:y'' = - g   ##############################
    v= - g
    return v

def fv(t):  ##################### Eq:  v = - gt   #############################
    v= - g*t
    return v

def fy(t):  ##################### Eq:  y = - 1/2*g*t^2  ######################  
    y= -(1/2)*g*(t**2)
    return y

# IC Varibles   ###############################################################   
ti=0      # initial time in seconds 
tf=10     # final time inseconds
yi=1000   # initial distance in meters
vi=0      # initial velocity in m/s
delta_t=np.array([1,.1,.01])   # delta t array
n=len(delta_t)                 # size of delta t array

# Main Body  ##################################################################

labe=np.array(["E:dt=1","E:dt=.1","E:dt=.01"])    # labels for Eular
labec=np.array(["EC:dt=1","EC:dt=.1","EC:dt=.01"])# labels for E.C.

# Foward Eular ################################################################
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)   # time array
    y  = np.zeros(nt)
    v  = np.zeros(nt)
    y[0] = yi
    v[0] = vi
    
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * v[k]
        v[k+1] = v[k] + delta_t[i] * F(t[k])
        t[k+1] = t[k] + delta_t[i]
        
    plt.plot(t,y,"-",label=labe[i]) # Plot E.

# Eular Cromwell ##############################################################
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)  # time array
    y  = np.zeros(nt)
    v  = np.zeros(nt)
    y[0] = yi
    v[0] = vi
    
    for k in range(nt-1):
        v[k+1] = v[k] + delta_t[i] * F(t[k])
        y[k+1] = y[k] + delta_t[i] * v[k+1]
        t[k+1] = t[k] + delta_t[i]
        
    plt.plot(t,y,"--",label=labec[i])   # Plot E.C.
# Analytical Solution : ########y(t) = yi - vi*t - 1/2*g*t^2 ##################

a = yi + fv(t)+ fy(t)

# Plotting ####################################################################
plt.plot(t,a,'-.',label='Analytical Sol')
plt.legend()
plt.xlabel("t (time in seconds)")
plt.ylabel("y (vertical distance in meters)")
plt.grid()
plt.title("Solutions to y''= -g using Eular and Eular Cromwell")

