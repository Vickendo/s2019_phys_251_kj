#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 20:07:51 2019

@author: kendalljohnson
"""
print("Homework 9 Question 1")


# Imports  ####################################################################
import numpy as np
import matplotlib.pyplot as plt

# ODE Function ################# y' = - gt ####################################

g= 9.8 # m/s^2
def f(t):
    y_prime=-g*t
    return y_prime

# Analytical Soultion (from Dif Eqs.) #### Eq:  y = -1/2 *g* t^2  #############
    
def fy(t): 
    y= yi -(1/2)*g*(t**2)
    return y

# IC Varibles #################################################################

ti=0
yi=1000
tf=10
delta_t=np.array([1,.1,.01])
n=len(delta_t)
lab=np.array(["dt=1","dt=.1","dt=.01"])

# Main Body ###################################################################

# Foward Euler Method 
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)
    y  = np.zeros(nt)
    y[0] = yi
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * f(t[k])
    plt.plot(t,y,label=lab[i]) # Plot

# Plotting ####################################################################
plt.plot(t,fy(t),'--',label='Analytical Sol')
plt.legend()
plt.xlabel("t (time in seconds)")
plt.ylabel("y (vertical distance in meters)")
plt.grid()
plt.title("Solutions to y'= - gt ")