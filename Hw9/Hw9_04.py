#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 20:30:31 2019

@author: kendalljohnson
"""

print("Homework 9 Question 4")

# Imports #####################################################################

import numpy as np
import matplotlib.pyplot as plt

# Defing the ODE Functions  ###################################################

# constants in fucnctions 
g= 9.8 # m/s^2 Earth's gravity constant
alpha = 45 # degrees
vi=10      # m/s

def py(t):  ##################### Eq:  y = vi x sin(alpha) - 1/2*g*t^2  ####### 
    y= vi*np.sin(alpha)*t-(1/2)*g*(t**2)
    return y

def vy(t):  ##################### Eq:  vy = vi*sin(alpha) - gt  ############### 
    v = vi*np.sin(alpha) - g*t
    return v

# IC Varibles   ###############################################################   
ti=0      # seconds
tf=10     # seconds
yi=0   # meters
vi=10      # m/s
delta_t=np.array([1,.1,.01])   # delta t array
n=len(delta_t)                 # size of delta t array

# Main Body  ##################################################################

labe=np.array(["E:dt=1","E:dt=.1","E:dt=.01"])    # labels for Eular
labvel=np.array(["Vel-Vel:dt=1","Vel-Vel:dt=.1","Vel-Vel:dt=.01"])# labels for Vel-Verlet

# Analytical Solution : y(t) = yi - vi*sin(aplha)*t - 1/2*g*t^2  -vi*sin(alpha) - g*t
a = yi + py(t) + vy(t)

# Foward Eular ################################################################
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)   # time array
    y  = np.zeros(nt)
    v  = np.zeros(nt)
    y[0] = yi
    v[0] = vi
    
    for k in range(nt-1):
        y[k+1] = y[k] + delta_t[i] * v[k]
        v[k+1] = v[k] + delta_t[i] * vy(t[k])
        t[k+1] = t[k] + delta_t[i]
    plt.figure(1)
    plt.plot(y,v,"-",label=labe[i]) # Plot E.

# Plotting ####################################################################
plt.plot(y,a,'--',label='Analytical Sol')
plt.legend()
plt.ylabel("v (vertical velocity in meters / seconds)")
plt.xlabel("y (vertical distance in meters)")
plt.grid()
plt.title("Using Euler's Method")

# Velocity Verlet #############################################################
for i in range(n):
    nt = int(((tf-ti)/delta_t[i])+1)
    t  = np.linspace(ti,tf,num=nt)  # time array
    y  = np.zeros(nt)
    v  = np.zeros(nt)
    y[0] = yi
    v[0] = vi
    
    for k in range(nt-1):
        y[k+1] = y[k] + v[k]*delta_t[i]+ (((delta_t[i])**2)/2) * vy(t[k+1])
        v[k+1] = v[k] + delta_t[i] * ((vy(t[k])+vy(t[k]))/2)
        t[k+1] = t[k] + delta_t[i]  
    plt.figure(2)
    plt.plot(y,v,"-",label=labvel[i])   # Plot Velocity Verlet

# Plotting ####################################################################
plt.plot(y,a,'--',label='Analytical Sol')
plt.legend()
plt.ylabel("v (vertical velocity in meters / seconds)")
plt.xlabel("y (vertical distance in meters)")
plt.grid()
plt.title("Solutions using Velocity Verlet")