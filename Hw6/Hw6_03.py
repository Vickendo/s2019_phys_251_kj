
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 13:39:42 2019

@author: kendalljohnson
"""

# Homework 6 problem 3
print("Homework 6 problem 3")

# Imports
import numpy as np
from scipy.integrate import quad
from math import pi

#Exact intergral

    
# Equation 1 
a=0.0
b=4.0
Int1=lambda x:x**4
A1,err=quad(Int1,a,b)
# Equation 2
c=3.0
Int2=lambda x:2/(x-4)
A2,err=quad(Int2,a,c)

# Equation 3
d=1.0
Int3=lambda x:(x**2)*(np.log(x))
A3,err=quad(Int3,d,b)

# Equation 4
e=2*pi
Int4=lambda x:np.exp(2*x)*np.sin(2*x)
A4,err=quad(Int4,a,e)

# output solutions to exact Area
print()
print('Numerical integration using scipy quad')
print()
print('Total Area of Eq 1 using quad = {}'.format(A1))
print('Total Area of Eq 2 using quad = {}'.format(A2))
print('Total Area of Eq 3 using quad = {}'.format(A3))
print('Total Area of Eq 4 using quad = {}'.format(A4))

# Part 2   ####################################################################

# Defined the function of Trapizoidal

# n's
n1=2
n2=10
n3=100
n4=1000

# Defined the function of Trapizoidal


def Simps(fun,a,b,n):
    dx=float((b-a)/n)
    x=np.linspace(a,b,n+1)                          # only evens
    y=fun(x)
    Area_T=0.0
    #num=x.size
    #for i in range(x):
        #(dx/6)*sum(y[a]+4*y[a+i]+y[b])
        
        
        #xmid=(x[1]+x[0]) / 2.0
#A[0]=dx/6.0*(Int1(x[0])+4.0*Int1(xmid)+Int1(x[1]))
#xmid=(x[2]+x[1]) / 2.0
#A[1]=dx/6.0*(Int1(x[1])+4.0*Int1(xmid)+Int1(x[2]))
        
        
        
    Area_T=(dx/6)*sum(y[0:-1:2]+4*y[1::2]+y[2::2])    # had trouble using a for-loop to get this equation to give correct values soon sliced
    Area_T=(Area_T)*2                                 # correcting 
    return Area_T


# output solutions to Calc Area
print()
print('Numerical integration using the Simpsons method for n=2') # had problems with 1 likely due to line 
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Simps(Int1, a, b, n1)))
print('Total Area under the graph for Eq 2 is {} units'.format(Simps(Int2, a, c, n1)))
print('Total Area under the graph for Eq 3 is {} units'.format(Simps(Int3, d, b, n1)))
print('Total Area under the graph for Eq 4 is {} units'.format(Simps(Int4, a, e, n1)))

print()
print('Numerical integration using the Simpsons method for n=10')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Simps(Int1, a, b, n2)))
print('Total Area under the graph for Eq 2 is {} units'.format(Simps(Int2, a, c, n2)))
print('Total Area under the graph for Eq 3 is {} units'.format(Simps(Int3, d, b, n2)))
print('Total Area under the graph for Eq 4 is {} units'.format(Simps(Int4, a, e, n2)))

print()
print('Numerical integration using the Simpsons method for n=100')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Simps(Int1, a, b, n3)))
print('Total Area under the graph for Eq 2 is {} units'.format(Simps(Int2, a, c, n3)))
print('Total Area under the graph for Eq 3 is {} units'.format(Simps(Int3, d, b, n3)))
print('Total Area under the graph for Eq 4 is {} units'.format(Simps(Int4, a, e, n3)))

print()
print('Numerical integration using the Simpsons method for n=1000')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Simps(Int1, a, b, n4)))
print('Total Area under the graph for Eq 2 is {} units'.format(Simps(Int2, a, c, n4)))
print('Total Area under the graph for Eq 3 is {} units'.format(Simps(Int3, d, b, n4)))
print('Total Area under the graph for Eq 4 is {} units'.format(Simps(Int4, a, e, n4)))