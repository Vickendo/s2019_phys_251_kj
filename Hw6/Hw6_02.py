
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 13:39:42 2019

@author: kendalljohnson
"""

# Homework 6 problem 2
print("Homework 6 problem 2")

# Imports
import numpy as np
from scipy.integrate import quad
from math import pi

#Exact intergral

    
# Equation 1 
a=0.0
b=4.0
Int1=lambda x:x**4
A1,err=quad(Int1,a,b)
# Equation 2
c=3.0
Int2=lambda x:2/(x-4)
A2,err=quad(Int2,a,c)

# Equation 3
d=1.0
Int3=lambda x:(x**2)*(np.log(x))
A3,err=quad(Int3,d,b)

# Equation 4
e=2*pi
Int4=lambda x:np.exp(2*x)*np.sin(2*x)
A4,err=quad(Int4,a,e)

# output solutions to exact Area
print()
print('Numerical integration using scipy quad')
print()
print('Total Area of Eq 1 using quad = {}'.format(A1))
print('Total Area of Eq 2 using quad = {}'.format(A2))
print('Total Area of Eq 3 using quad = {}'.format(A3))
print('Total Area of Eq 4 using quad = {}'.format(A4))

# Part 2   ####################################################################

# Defined the function of Trapizoidal

# n's
n1=1
n2=10
n3=100
n4=1000

# Defined the function of Trapizoidal


def Trap(fun, a, b, n): # Trap Equation : dx* (y(a)+y(b))/2
    dx= (b-a)/float(n)
    Area_T=0.0
    Area_T+=fun(a)/2.0            # side 1st
    for i in range(1, n):
        Area_T+=fun(a+i*dx)       # side all middle y lengths
    Area_T+=fun(b)/2.0            # side last
    return Area_T*dx

# output solutions to Calc Area
print()
print('Numerical integration using the Trapizoidal method for n=1')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Trap(Int1, a, b, n1)))
print('Total Area under the graph for Eq 2 is {} units'.format(Trap(Int2, a, c, n1)))
print('Total Area under the graph for Eq 3 is {} units'.format(Trap(Int3, d, b, n1)))
print('Total Area under the graph for Eq 4 is {} units'.format(Trap(Int4, a, e, n1)))

print()
print('Numerical integration using the Trapizoidal method for n=10')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Trap(Int1, a, b, n2)))
print('Total Area under the graph for Eq 2 is {} units'.format(Trap(Int2, a, c, n2)))
print('Total Area under the graph for Eq 3 is {} units'.format(Trap(Int3, d, b, n2)))
print('Total Area under the graph for Eq 4 is {} units'.format(Trap(Int4, a, e, n2)))

print()
print('Numerical integration using the Trapizoidal method for n=100')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Trap(Int1, a, b, n3)))
print('Total Area under the graph for Eq 2 is {} units'.format(Trap(Int2, a, c, n3)))
print('Total Area under the graph for Eq 3 is {} units'.format(Trap(Int3, d, b, n3)))
print('Total Area under the graph for Eq 4 is {} units'.format(Trap(Int4, a, e, n3)))

print()
print('Numerical integration using the Trapizoidal method for n=1000')
print()
print('Total Area under the graph for Eq 1 is {} units'.format(Trap(Int1, a, b, n4)))
print('Total Area under the graph for Eq 2 is {} units'.format(Trap(Int2, a, c, n4)))
print('Total Area under the graph for Eq 3 is {} units'.format(Trap(Int3, d, b, n4)))
print('Total Area under the graph for Eq 4 is {} units'.format(Trap(Int4, a, e, n4)))