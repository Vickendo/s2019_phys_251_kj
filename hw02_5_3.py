#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 07:33:04 2019

@author: kendalljohnson
"""
#Question 5
#1
import math
import matplotlib.pyplot as plt
import numpy as np









pi = math.pi
t = np.arange(0,4*(pi),(pi))
w=np.sin(t)
#plt.plot(t,w)
plt.plot(t,w,'.',label='$f(x)=sin(x)$',color='k')
plt.grid(color='red')
plt.xlabel("Input")
plt.ylabel("Output")
plt.legend()
plt.title('The Equation of the sin of x')