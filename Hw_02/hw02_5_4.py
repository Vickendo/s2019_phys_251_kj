#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 07:40:31 2019

@author: kendalljohnson
"""


#Question 5
#1
import math
import matplotlib.pyplot as plt
import numpy as np









pi = math.pi

#plt.plot(t,w)


a = 0.1
o = np.arange(-8*pi,8*pi,pi/4)
r = np.exp(a*o)


#plt.plot(o,r)
plt.plot(o,r,'+',label='$f(a)=e^(Oa)$',color='k')
plt.grid(color='green')
plt.xlabel("Input")
plt.ylabel("Output")
plt.legend()
plt.title('The Equation of e to the power theta times a')