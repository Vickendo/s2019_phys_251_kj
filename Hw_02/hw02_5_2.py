#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 07:27:13 2019

@author: kendalljohnson
"""

import math
import matplotlib.pyplot as plt
import numpy as np





pi = math.pi
d = np.arange(0,4*pi,pi/4)
Q = np.sin(d)
plt.plot(d,Q,'.',label='$f(x)=sin(x)$')
plt.grid(color='gray')
plt.xlabel("input")
plt.ylabel("output")
plt.legend()
plt.title('The Equation of the sin of x')