#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 07:26:11 2019

@author: kendalljohnson
"""

#Question 5
#1
#import math
import matplotlib.pyplot as plt
import numpy as np


x = np.arange(1,10.5,.5)
y = x**2
plt.plot(x,y,'.',label='$f(x)=x^2$')
plt.grid(color='gray')
plt.xlabel("Input")
plt.ylabel("Output")
plt.legend()
plt.title('The Equation of x to the power of 2')