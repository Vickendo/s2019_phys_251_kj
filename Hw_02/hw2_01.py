#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 18:48:02 2019

@author: kendalljohnson
"""

#Homework 2 Question 1
print() #Making space for more clearly printed information
print("Question 1 part 1")
print() #Making space for more clearly printed information
n=5
m=10
i=50
j=100
N=500
S=0
for k in range(0,n):
    S += 1/(2**k)
    
print("The Sum of the Function 1 is {0:} when n is {1:}".format(S,n))
print("The Sum of the Function 1 is {0:} when n is {1:}".format(S,m))
print("The Sum of the Function 1 is {0:} when n is {1:}".format(S,i))
print("The Sum of the Function 1 is {0:} when n is {1:}".format(S,j))
print("The Sum of the Function 1 is {0:} when n is {1:}".format(S,N))
#Homework 2 Question 2
print() #Making space for more clearly printed information
print("Question 1 part 2")
print() #Making space for more clearly printed information


for k in range(2,n):
    S += 4/(3**k)
    
print("The Sum of the Function 2 is {0:10f} when n is {1:}".format(S,n))
print("The Sum of the Function 2 is {0:10f} when n is {1:}".format(S,m))
print("The Sum of the Function 2 is {0:10f} when n is {1:}".format(S,i))
print("The Sum of the Function 2 is {0:10f} when n is {1:}".format(S,j))
print("The Sum of the Function 2 is {0:10f} when n is {1:}".format(S,N))

#Homework 2 Question 3
print() #Making space for more clearly printed information
print("Question 1 part 3")
print() #Making space for more clearly printed information


for k in range(1,n):
    S += ((3/2**k)-2/(5**k))
    
print("The Sum of the Function 3 is {0:10f} when n is {1:}".format(S,n))
print("The Sum of the Function 3 is {0:10f} when n is {1:}".format(S,m))
print("The Sum of the Function 3 is {0:10f} when n is {1:}".format(S,i))
print("The Sum of the Function 3 is {0:10f} when n is {1:}".format(S,j))
print("The Sum of the Function 3 is {0:10f} when n is {1:}".format(S,N))