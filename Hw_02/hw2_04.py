#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 17 11:02:13 2019

@author: kendalljohnson
"""

#Question 4 
#import random
import numpy as np
#from math import pi

#Question 4 part 1
print() #Making space for more clearly printed information
print("Question 4 part 1")
print() #Making space for more clearly printed information

x=10
t=10

A=np.empty([x,t],dtype='float')
B=np.empty([x,t],dtype='float')
for i in range(0,x):
    for j in range(0,t):
        n=i*2 + j*2
        A[i,j]=n
        B[i,j]=n
        
print(A)

#############################################

#Question 4 part 2

print() #Making space for more clearly printed information
print("Question 4 part 2")
print() #Making space for more clearly printed information
print("3rd Row of random intergers from 7 to 10")
print() #Making space for more clearly printed information
print(A[2,:])
print() #Making space for more clearly printed information
print("3rd Column of random intergers from 7 to 10")
print() #Making space for more clearly printed information
print(A[:,2])
####################################################

#Question 4 part 3

print() #Making space for more clearly printed information
print("Question 4 part 3")
print() #Making space for more clearly printed information
#print() #Making space for more clearly printed information
print("99's in last column")
print() #Making space for more clearly printed information

A[:,9]=99
print(A)
####################################################
#Question 4 part 4

print() #Making space for more clearly printed information
print("Question 4 part 4")
print() #Making space for more clearly printed information
B[B==8]=-1

print(B)