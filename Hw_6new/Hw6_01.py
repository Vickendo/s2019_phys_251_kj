#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 11:31:36 2019

@author: kendalljohnson
"""

# Homework 6 problem 1
print("Homework 6 problem 1")
print()

# Imports

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

# Exact intergral using scipy intergrate

a=0.0
b=1.0
Int=lambda x:x**2
At,err=quad(Int,a,b)

# Output solution to exact area from scipy intergrate

print('Total Area with scipy quad = {}'.format(At))
print()

# Varibles and Value array

n=1000
x=np.linspace(a,b,n+1)
num=x.size
y=np.zeros(num)
Area=np.zeros(num)
Area_T=0.0

# for loops

dx=(b-a)/(float(num))
for i in range(num-1):
    y[i]=Int((x[i+1]+x[i])/2)
    Area[0]=((b-a)/float(1))*y[0]
    Area[i]=dx*y[i]
    Area_T+=Area[i]

# output solution
    
print('Numerical integration using the MIDDLE RECTANGLE method')
print()

D=np.abs((Area_T-At)/At)
print(Area_T)    # Had trouble iterating n and Area_T. I was able to get the correct answer for the given value n but unable to plot
#x=x[:-1]
#D=D[:-1]

# Ploting

plt.plot(n,D,".",label="Line")
plt.legend()
plt.title('Area of increase')
plt.xlabel("The growing index n")
plt.ylabel("The difference in Areas")
plt.grid()