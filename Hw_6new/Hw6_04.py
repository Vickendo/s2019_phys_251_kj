#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 14 20:05:33 2019

@author: kendalljohnson
"""
# Homework 6 problem 4

print("Homework 6 problem 4")

# Imports

import numpy as np
from scipy.integrate import quad

#Exact intergral using scipy quad 

a=0.0
b=4.0
Int1=lambda x:((x**3)/81)+1
A1,err=quad(Int1,a,b)

print()
print('Numerical integration using scipy quad')
print('Total Area of the Eq using quad is = {} units'.format(A1))

# Trapizoidal Method

def Trap(fun, a, b, n):
    dx= (b-a)/float(n)
    Area_T=0.0
    Area_T+=fun(a)/2.0            # side 1st
    for i in range(1, n):
        Area_T+=fun(a+i*dx)       # side all middle y lengths
    Area_T+=fun(b)/2.0            # side last
    return Area_T*dx

# output solutions to Calc Area
    
print()
print('Numerical integration using the Trapizoidal method for 2 Areas')
print('Total Area under the graph for the Eq is {} units'.format(Trap(Int1, a, b, 2)))

def Simps(fun,a,b,n):
    dx=float((b-a)/n)
    x=np.linspace(a,b,n+1)                          # only evens
    y=fun(x)
    Area_T=0.0
    Area_T=(dx/6)*sum(y[0:-1:2]+4*y[1::2]+y[2::2])    
    Area_T=(Area_T)*2                                 # correcting 
    return Area_T



print()
print('Numerical integration using the Simpsons method for 2 Areas') # had problems with 1 likely due to line 
print('Total Area under the graph for the Eq is {} units'.format(Simps(Int1, a, b, 2)))

# The difference between Trap est and Simps est
D=np.abs((Trap(Int1, a, b, 2)-Simps(Int1, a, b, 2))/Simps(Int1, a, b, 2))
print("The Difference is {}".format(D))
