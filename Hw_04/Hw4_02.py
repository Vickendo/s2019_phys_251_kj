#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 10:48:32 2019

@author: kendalljohnson
"""

# Question 2 of Homework 4

# Imports

import pandas as pd  
import matplotlib.pyplot as plt
#from scipy.optimize import curve_fit
import numpy as np

# Optimization curve fit line

#earth.csv
#sun(1).csv
#H_65_RC.csv

#Pull up and use data

dname=r'class_data_01'                      
fname='earth.csv'                   
dfname ='{0:s}/{1:s}'.format(dname,fname)    
data01 = pd.read_csv(dfname)    
x=data01.values[:,0]
y=data01.values[:,1] 
z=data01.values[:,2]
 

# coordinate matrices
xx, yy = np.meshgrid(x, y)  ## creates areas like on board in class
# calculate z=f(x,y)


# plot the surface defined by (x,y,z)

# Add the necessary modules to plot
#from mpl_toolkits.mplot3d import Axes3D # to plot a 3D object you must import this module!!!   
#import matplotlib.pyplot as plt         # usual plot module
from matplotlib import cm               # module to have access to different color maps

# Plot the surface
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(xx, yy, z, cmap=cm.terrain,linewidth=0, antialiased=False)
plt.show()
