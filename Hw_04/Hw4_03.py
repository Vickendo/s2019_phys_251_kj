#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 08:04:58 2019

@author: kendalljohnson
"""

# Question 3 from homework 4

# Imports

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.optimize as so

#Pull up and use data

dname=r'class_data_01'
fname=r'free_fall_run18_csv.csv'
dfname='{0:s}/{1:s}'.format(dname,fname)
data3=pd.read_csv(dfname)
x=data3.values[:,0]
y=data3.values[:,1]

#Individual Parabola's

#Parabola 1
x1 = x[30:48]
y1 = y[30:48]

#Parabola 2
x2 = x[48:78]
y2 = y[48:78]

#Parabola 3
x3 = x[78:106]
y3 = y[78:106]

#Parabola 4
x4 = x[105:127]
y4 = y[105:127]

#Parabola 5
x5 = x[127:147]
y5 = y[127:147]

#Parabola 6
x6 = x[146:163]
y6 = y[146:163]

#Parabola 7
x7 = x[162:178]
y7 = y[162:178]

#Parabola 8
x8 = x[178:189]
y8 = y[178:189]

# Optimization curve fit line

def Parab(x, a, b, c):
    return (x**2)*a+x*b+c

# Optimization parameters
    
popt1, pcov1 = so.curve_fit(Parab,x1,y1)
y01=Parab(x1,popt1[0],popt1[1],popt1[2])

popt2, pcov2 = so.curve_fit(Parab,x2,y2)
y02=Parab(x2,popt2[0],popt2[1],popt2[2])

popt3, pcov3 = so.curve_fit(Parab,x3,y3)
y03=Parab(x3,popt3[0],popt3[1],popt3[2])

popt4, pcov4 = so.curve_fit(Parab,x4,y4)
y04=Parab(x4,popt4[0],popt4[1],popt4[2])

popt5, pcov5 = so.curve_fit(Parab,x5,y5)
y05=Parab(x5,popt5[0],popt5[1],popt5[2])

popt6, pcov6 = so.curve_fit(Parab,x6,y6)
y06=Parab(x6,popt6[0],popt6[1],popt6[2])

popt7, pcov7 = so.curve_fit(Parab,x7,y7)
y07=Parab(x7,popt7[0],popt7[1],popt7[2])

popt8, pcov8 = so.curve_fit(Parab,x8,y8)
y08=Parab(x8,popt8[0],popt8[1],popt8[2])

# Plotting

plt.plot(x1,y01,'b-', label='Curve fit line')
plt.plot(x1,y1,'.',color='r',label='Parabola 1')

plt.plot(x2,y02,'b-')
plt.plot(x2,y2,'.',color='r',label='Parabola 2')

plt.plot(x3,y03,'b-')
plt.plot(x3,y3,'.',color='y',label='Parabola 3')

plt.plot(x4,y04,'b-')
plt.plot(x4,y4,'.',color='g',label='Parabola 4')

plt.plot(x5,y05,'b-')
plt.plot(x5,y5,'.',color='c',label='Parabola 5')

plt.plot(x6,y06,'b-')
plt.plot(x6,y6,'.',color='b',label='Parabola 6')

plt.plot(x7,y07,'b-')
plt.plot(x7,y7,'.',color='m',label='Parabola 7')

plt.plot(x8,y08,'b-')
plt.plot(x8,y8,'.',color='k',label='Parabola 8')

# More Plotting

plt.legend()
plt.minorticks_on()
plt.xlabel('Time (seconds)')
plt.ylabel('Position (meters)')
plt.grid()
plt.title('Balls position changing in time')


# Constants Of Gravity

#from the Curve fit

g1=np.mean(popt1[:]*2)
g2=np.mean(popt2[:]*2)
g3=np.mean(popt3[:]*2)
g4=np.mean(popt4[:]*2)
g5=np.mean(popt5[:]*2)
g6=np.mean(popt6[:]*2)
g7=np.mean(popt7[:]*2)
g8=np.mean(popt8[:]*2)



g=[g2/2,g3/2,g4/2,g5/2,g6/2,g7/2,g8/2]

# took off g1 from data due to half of parabola
n=7
G=0.0
for i in range(n):
    G=G+g[i]

xdash=G/n
print('The avg gravity is computed to be {} m/s*2'.format(xdash))
xi=0.0

for i in range(n):
    xi=xi+(g[i]-xdash)**2   

O=(xi/(n-1))**(1/2)
print("The standard deviation is {}".format(O))
# The standard div seemed a high, but the last gravity calcs are high also