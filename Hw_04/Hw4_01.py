#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 19:32:19 2019

@author: kendalljohnson
"""

#Question 1 of Homework 4

#Imports

import matplotlib.pyplot as plt
import scipy.optimize as so
import pandas as pd
import numpy as np

#Pull up and use data

dname=r'class_data_01'

# Data 10
f1name='dat_10.txt'
df1name='{0:s}/{1:s}'.format(dname,f1name)
data10 = pd.read_csv(df1name)
# Data 11
f2name='dat_11.txt'
df2name='{0:s}/{1:s}'.format(dname,f2name)
data11 = pd.read_csv(df2name)
# Data 12
f3name='dat_12.txt'
df3name='{0:s}/{1:s}'.format(dname,f3name)
data12 = pd.read_csv(df3name)

#I tried to index the change from data10 to data 11,but I ended manually changing in these lines of code
x=data10.values[:,0]
y=data10.values[:,1]

# On the file I manually added commas which aloud me to work with the file

# Optimization curve fit line

def myline(x,m,b):
    y=m*x+b
    return y

# Summations 
n=x.size
Sx=0.0
Sy=0.0
Sxy=0.0
Sx2=0.0
R2a=0.0
R2b=0.0

for i in range(n):
    Sx=Sx+x[i]
    Sy=Sy+y[i]
    Sxy=Sxy+x[i]*y[i]
    Sx2=Sx2+x[i]*x[i]
    
# Varibles for the line  manually made line   
    
m=(n*Sxy-Sx*Sy)/(n*(Sx2)-Sx**2)
b=(Sx2*Sy-Sxy*Sx)/(n*Sx2-Sx*Sx)
ybar=Sy/n
yhat=(m*x+b)

for i in range(n):
    R2a=R2a + ((m*x[i]+b)-y[i])**2
    R2b=R2b + (ybar-y[i])**2

R2=1-(R2a/R2b)
print("The Slope on the data set is {}".format(m))
print("The Intercept of the data set is {}".format(b))
print("The Khi of the line is {}".format(np.mean(R2)))

# Optimization parameter 

popt0,pcov0 = so.curve_fit(myline,x,y)
print('Optimization parameter:')
print(popt0)
print('Covariance matrix:')
print(pcov0)
m0=popt0[0]   
b0=popt0[1]
yhat0=m0*x+b0

# Plotting

plt.plot(x,yhat0,'r',label='Curve fit line')
plt.plot(x,y,'d',color='k',label='Raw_Data')
plt.plot(x,yhat,'b-', label='Calculated fit')
#Curve fit line fits perfectly over manually made line
plt.legend()
plt.minorticks_on()
plt.xlabel('Time (seconds)')
plt.ylabel('Position (meters)')
plt.grid()
plt.title('Position Changing in time')